mongodb
=======

Simple mongodb install

Requirements
------------

No known pre-requisites.

Role Variables
--------------

Dependencies
------------

No dependencies.

Example Playbook
----------------

```
- hosts: servers
  roles:
     - { role: bencooling.mongodb }
```

License
-------

BSD

Author Information
------------------

[bcooling.com.au](bcooling.com.au)
